#include "counter.h"

void counter_init(counter *c) {
  memset(c, 0, sizeof(*c));
}

void count_bytes(filebuf *fb, counter *c) {
  while (fb_avail(fb)) {
    uint8_t byte = fb_readbyte(fb);
    (*c)[byte]++;
  }
  fb_reset(fb);
}

uint64_t message_len(counter *c) {
  uint64_t res = 0;
  for (int i = 0; i < 256; i++)
    res += (*c)[i];
  return res;
}
