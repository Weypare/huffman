#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "btree.h"
#include "counter.h"
#include "filebuf.h"
#include "pqueue.h"
#include "util.h"

int main(int argc, char **argv) {
  if (argc < 3) {
    help(argv[0]);
    return 0;
  }
  for (int i = 1; i < argc; i++) {
    if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0) {
      help(argv[0]);
      return 0;
    }
  }

  FILE *infile = fopen(argv[1], "rb");
  FILE *outfile = fopen(argv[2], "wb");

  if (!infile || !outfile) {
    fprintf(stderr, "Unable to open file");
    return -1;
  }

  filebuf *infile_buf = fb_new(infile);
  filebuf *outfile_buf = fb_new(outfile);

  uint64_t element_count = 0;
  uint64_t msg_len = 0;

  fb_readbytes((char *)&element_count, sizeof(element_count), 1, infile_buf);
  node *tree = read_tree(infile_buf, element_count);
  fb_readbytes((char *)&msg_len, sizeof(msg_len), 1, infile_buf);

  while (msg_len) {
    node *cur = tree;
    while (cur->left || cur->right) {
      if (fb_readbit(infile_buf) == 0)
        cur = cur->left;
      else
        cur = cur->right;
    }
    fb_writebyte(outfile_buf, cur->value);
    msg_len--;
  }

  fb_write(outfile_buf);

  node_delete(tree);
  fb_close(infile_buf);
  fb_close(outfile_buf);

  return 0;
}
