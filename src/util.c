#include "util.h"

void write_tree(filebuf *fb, node *root) {
  if (!root)
    return;
  if (root->left) {
    fb_writebit(fb, 0);
    write_tree(fb, root->left);
  }

  if (!root->internal) {
    fb_writebit(fb, 1);
    for (int i = 0; i < 8; i++) {
      fb_writebit(fb, 0 != (root->value & (1 << (7 - i))));
    }
  }

  if (root->right) {
    fb_writebit(fb, 0);
    write_tree(fb, root->right);
  }
}

node *create_tree(counter *c) {
  heap *queue = (heap *)calloc(1, sizeof(heap));
  for (uint64_t i = 0; i < sizeof(*c) / sizeof((*c)[0]); i++)
    if ((*c)[i] != 0) {
      node *n = node_new(i, (*c)[i]);
      pq_push(queue, n);
    }
  while (queue->len > 1) {
    node *a = pq_pop(queue);
    node *b = pq_pop(queue);
    node *internal = node_new_internal(a, b);
    pq_push(queue, internal);
  }
  node *res = pq_pop(queue);
  pq_delete(queue);
  return res;
}

void inorder_traverse(node *root, const char *s, char dict[256][40]) {
  if (!root)
    return;

  char new_s[256];
  int length = strlen(s);
  memset(new_s, 0, 256);
  strncpy(new_s, s, length);

  new_s[length] = '0';
  inorder_traverse(root->left, new_s, dict);

  if (!root->internal) {
    strncpy(dict[(uint8_t)root->value], s, strlen(s));
  }

  new_s[length] = '1';
  inorder_traverse(root->right, new_s, dict);
}

static void read_tree_helper(filebuf *fb, node *n, int *element_count) {
  if (*element_count == 0)
    return;
  if (n->left && n->right)
    return;

  while (1) {
    if (fb_readbit(fb) == 0) {
      if (!n->left) {
        n->left = node_new_internal(NULL, NULL);
        read_tree_helper(fb, n->left, element_count);
      } else if (!n->right) {
        n->right = node_new_internal(NULL, NULL);
        read_tree_helper(fb, n->right, element_count);
      } else {
        break;
      }
    } else {
      (*element_count)--;
      char c = 0;
      fb_readbytes(&c, 1, 1, fb);
      n->value = c;
      n->prob = 1;
      break;
    }

    if (n->left && n->right)
      return;

    if (*element_count == 0)
      return;
  }
}

node *read_tree(filebuf *fb, int element_count) {
  node *root = node_new_internal(NULL, NULL);
  read_tree_helper(fb, root, &element_count);
  return root;
}

void help(char *name) {
  printf("Usage: %s <input> <output>\n", name);
  printf("\t-h, --help \t show this message\n");
}
