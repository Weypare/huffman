#include <stdio.h>
#include <string.h>

#include "btree.h"
#include "counter.h"
#include "filebuf.h"
#include "pqueue.h"
#include "util.h"

int main(int argc, char **argv) {
  if (argc < 3) {
    help(argv[0]);
    return 0;
  }
  for (int i = 1; i < argc; i++) {
    if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0) {
      help(argv[0]);
      return 0;
    }
  }

  FILE *infile = fopen(argv[1], "rb");
  FILE *outfile = fopen(argv[2], "wb");

  if (!infile || !outfile) {
    fprintf(stderr, "Unable to open file");
    return -1;
  }

  filebuf *infile_buf = fb_new(infile);
  filebuf *outfile_buf = fb_new(outfile);

  counter count;
  counter_init(&count);
  count_bytes(infile_buf, &count);

  node *tree = create_tree(&count);

  char replace_dict[256][40];
  for (int i = 0; i < 256; i++)
    replace_dict[i][0] = 0;

  inorder_traverse(tree, "", replace_dict);

  uint64_t element_count = 0;
  for (int i = 0; i < 256; i++)
    if (count[i] != 0) {
      element_count++;
    }

  fb_writebytes((char *)&element_count, sizeof(element_count), 1, outfile_buf);
  write_tree(outfile_buf, tree);

  uint64_t msg_len = message_len(&count);
  fb_writebytes((char *)&msg_len, sizeof(msg_len), 1, outfile_buf);

  while (msg_len) {
    uint8_t c = fb_readbyte(infile_buf);
    char *ptr = replace_dict[c];
    while (*ptr != 0) {
      fb_writebit(outfile_buf, *ptr - '0');
      ptr++;
    }
    msg_len--;
  }

  fb_write(outfile_buf);

  node_delete(tree);
  fb_close(infile_buf);
  fb_close(outfile_buf);

  return 0;
}
