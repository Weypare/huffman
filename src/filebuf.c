#include "filebuf.h"

filebuf *fb_new(FILE *f) {
  filebuf *fb = (filebuf *)malloc(sizeof(filebuf));
  fb->file = f;
  fb->size = 0;
  fb->index = 0;
  fb->cur_bit = 0;

  return fb;
}

void fb_close(filebuf *fb) {
  fclose(fb->file);
  free(fb);
}

void fb_read(filebuf *fb) {
  memset(fb->buffer, 0, BUF_SIZE);
  fb->size = fread(fb->buffer, sizeof(char), BUF_SIZE, fb->file);
  fb->index = 0;
  fb->cur_bit = 0;
}

uint8_t fb_readbyte(filebuf *fb) {
  if (fb->index >= fb->size)
    fb_read(fb);
  char byte = fb->buffer[fb->index];
  fb->index++;
  return byte;
}

void fb_readbytes(void *dst, int size, int count, filebuf *fb) {
  for (int i = 0; i < count * size; i++) {
    char byte = 0;
    for (int j = 0; j < 8; j++) {
      char bit = (0 != fb_readbit(fb));
      byte |= bit << (7 - j);
    }
    ((char *)dst)[i] |= byte;
  }
}

inline uint8_t fb_readbit(filebuf *fb) {
  if (fb->cur_bit >= 8) {
    fb->cur_bit = 0;
    fb->index++;
  }
  if (fb->index >= fb->size)
    fb_read(fb);

  char byte = fb->buffer[fb->index];
  char bit = byte & (1 << (7 - fb->cur_bit));
  fb->cur_bit++;
  return bit;
}

void fb_write(filebuf *fb) {
  while (fb->size < fb->index)
    fb->size += fwrite(fb->buffer, sizeof(char), fb->index + (fb->cur_bit != 0),
                       fb->file);
  fb->size = 0;
  memset(fb->buffer, 0, BUF_SIZE);
  fb->index = 0;
  fb->cur_bit = 0;
}

void fb_writebyte(filebuf *fb, char byte) {
  if (fb->index >= BUF_SIZE)
    fb_write(fb);
  fb->buffer[fb->index] = byte;
  fb->index++;
}

void fb_writebytes(char *bytes, int size, int count, filebuf *fb) {
  for (int i = 0; i < count * size; i++)
    for (int j = 0; j < 8; j++)
      fb_writebit(fb, 0 != (bytes[i] & (1 << (7 - j))));
}

inline void fb_writebit(filebuf *fb, char bit) {
  if (fb->cur_bit >= 8) {
    fb->index++;
    fb->cur_bit = 0;
    if (fb->index >= BUF_SIZE)
      fb_write(fb);
  }
  fb->buffer[fb->index] |= ((bit != 0) << (7 - fb->cur_bit));
  fb->cur_bit++;
}

inline void fb_reset(filebuf *fb) {
  fseek(fb->file, 0, SEEK_SET);
  fb->index = 0;
  fb->size = 0;
  fb->cur_bit = 0;
  memset(fb->buffer, 0, BUF_SIZE);
}

inline int fb_eof(filebuf *fb) {
  return feof(fb->file);
}

inline int fb_avail(filebuf *fb) {
  return !fb_eof(fb) || (fb->index < fb->size);
}

inline int fb_curbit(filebuf *fb) {
  return fb->cur_bit;
}
