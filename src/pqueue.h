#pragma once

#include "btree.h"

typedef struct heap_ {
  node **nodes;
  int len;
  int size;
} heap;

void pq_push(heap *, node *);
node *pq_pop(heap *);
void pq_delete(heap *);
