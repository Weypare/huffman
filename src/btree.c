#include "btree.h"

node *node_new(char value, int prob) {
  node *n = (node *)malloc(sizeof(node));
  n->value = value;
  n->left = n->right = 0;
  n->prob = prob;
  n->internal = 0;
  return n;
}

node *node_new_internal(node *l, node *r) {
  node *n = (node *)malloc(sizeof(node));
  n->left = l;
  n->right = r;
  n->internal = 1;
  return n;
}

void node_delete(node *n) {
  if (!n)
    return;
  node_delete(n->left);
  node_delete(n->right);
  free(n);
}

uint64_t node_get_priority(node *n) {
  if (n->internal)
    return node_get_priority(n->left) + node_get_priority(n->right);
  return n->prob;
}
