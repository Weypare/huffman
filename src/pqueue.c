#include "pqueue.h"

void pq_push(heap *h, node *n) {
  if (h->len + 1 >= h->size) {
    h->size = h->size ? h->size * 2 : 4;
    h->nodes = (node **)realloc(h->nodes, h->size * sizeof(node *));
  }
  int i = h->len + 1;
  int j = i / 2;
  while (i > 1 && node_get_priority(h->nodes[j]) > node_get_priority(n)) {
    h->nodes[i] = h->nodes[j];
    i = j;
    j = j / 2;
  }
  h->nodes[i] = n;
  h->len++;
}

node *pq_pop(heap *h) {
  int i, j, k;
  if (!h->len) {
    return NULL;
  }
  node *data = h->nodes[1];

  h->nodes[1] = h->nodes[h->len];

  h->len--;

  i = 1;
  while (i != h->len + 1) {
    k = h->len + 1;
    j = 2 * i;
    if (j <= h->len &&
        node_get_priority(h->nodes[j]) < node_get_priority(h->nodes[k])) {
      k = j;
    }
    if (j + 1 <= h->len &&
        node_get_priority(h->nodes[j + 1]) < node_get_priority(h->nodes[k])) {
      k = j + 1;
    }
    h->nodes[i] = h->nodes[k];
    i = k;
  }
  return data;
}

void pq_delete(heap *h) {
  free(h->nodes);
  free(h);
}
