#pragma once

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> // memset

#ifndef BUF_SIZE
#define BUF_SIZE 1024 * 1024
#endif

typedef struct filebuf_ {
  FILE *file;
  uint8_t buffer[BUF_SIZE];
  size_t index;
  int cur_bit;
  size_t size;
} filebuf;

filebuf *fb_new(FILE *fd);
void fb_close(filebuf *fb);

void fb_read(filebuf *fb);
uint8_t fb_readbyte(filebuf *fb);
void fb_readbytes(void *buf, int size, int n, filebuf *fb);
uint8_t fb_readbit(filebuf *fb);

void fb_write(filebuf *fb);
void fb_writebyte(filebuf *fb, char byte);
void fb_writebytes(char *buf, int size, int n, filebuf *fb);
void fb_writebit(filebuf *fb, char bit);

void fb_reset(filebuf *fb);
int fb_eof(filebuf *fb);
int fb_avail(filebuf *fb);
int fb_curbit(filebuf *fb);
