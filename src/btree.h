#pragma once

#include <stdint.h>
#include <stdlib.h>

typedef struct node_ {
  struct node_ *left;
  struct node_ *right;
  int internal;
  uint8_t value;
  uint64_t prob;
} node;

node *node_new(char, int);
node *node_new_internal(node *, node *);
void node_delete(node *);
uint64_t node_get_priority(node *);
