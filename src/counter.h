#pragma once

#include <stdint.h>
#include <string.h> // memset

#include "filebuf.h"

typedef uint64_t counter[256];

void counter_init(counter *c);
void count_bytes(filebuf *fb, counter *c);
uint64_t message_len(counter *c);
