#pragma once

#include "btree.h"
#include "counter.h"
#include "filebuf.h"
#include "pqueue.h"

node *create_tree(counter *);
void inorder_traverse(node *, const char *, char[256][40]);
void write_tree(filebuf *, node *);
node *read_tree(filebuf *, int);
void help(char *);
