# Huffman coding

This is an implementation of Huffman encoder/decoder. It was made in educational purposes. The code can be used for reference and a base for modification and extension.

The encoder runs in 2 passes through the input file. The first will generate the Huffman tree. The second will encode the file. The header of the file will contain some data in order to decode.

The decoder runs in 1 pass. Firstly it reads the file header, which contains the
number of elements in Huffman tree, the Huffman Tree and the length of the initial message. Then it proceeds to decoding the file.


### Building:
```
meson build
cd build
ninja
```

### Usage:
```
encode <input> <output> 
decode <input> <output> 
```
